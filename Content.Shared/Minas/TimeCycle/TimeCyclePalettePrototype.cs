using Robust.Shared.Prototypes;

namespace Content.Shared.Minas.TimeCycle;

/// <summary>
/// Used for any announcements on the start of a round.
/// </summary>
[Prototype("timeCyclePalette")]
public sealed partial class TimeCyclePalettePrototype : IPrototype
{
    [IdDataField]
    public string ID { get; private set; } = default!;

    [DataField]
    public Dictionary<int, Color> Colors = default!;
}

